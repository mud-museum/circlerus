UTF-8 text:

CIRCLERUS MUD

(CircleMUD, частично русифицированный кем-то много лет назад)
-------------------------------------------------------------
и лежащий на многих сайтах в файлах под именами вида circlerus.zip / circlerus.tgz

23 января 2023 года, Харьков, Украина.

Внезапно я собрал этот мад для macOS. И буду дальше тестировать его в разных ОС.

Как собирать в macOS или Ubuntu:

make в каталоге src.

configure делать не надо, потому что после configure я менял флаги 
в Makefile. И кое-что менял в исходниках, удалял устаревшее в основном.
Мои изменения помечены словами prool или prool fool. (fool - это
"дурацкие" изменения, когда я резал код по живому, методом грубого хака)

Этот код кем-то частично русифицирован (очень частично), например
имя надо вводить только латинскими буквами. Код очень старый и имеет историческое
музейное значение.

Первый зарегистрированный чар будет иммортал, остальные обычные игроки.
Зона иммов не переведена, начальная зона игроков частично переведена.

Кодировка мада koi-8r (исходников, файлов данных и оригинальных доков).

Поэтому в последних версиях tintin++ надо вот такую команду вводить
каждый раз, когда входишь

#config charset KOI8TOUTF8

В моем клиенте prooltin (этой мой мод tintin++) вводится команда более простая #proolcoder
и один раз :)

Prool

proolix@gmail.com
http://mud.kharkov.org

----


CircleMUD 3.00 README file

Welcome to CircleMUD 3.0!  I hope you enjoy your stay.

Use of this software in any capacity implies that you have read, understood,
and agreed to abide by the terms and conditions set down by the CircleMUD
license contained in the file license.doc.

Also, out of courtesy if nothing else, please keep the 'credits' file intact.
You can add your own credits on top of the existing file, but I'd appreciate
it if you would not simply remove it and all references to the word "Circle"
everywhere in the MUD.

Jeremy Elson
jelson@jhu.edu

----------------------------------------------------------------------------

CircleMUD QuickStart Guide

If you are an experienced MUD player and you know your way around a compiler,
you can probably get up and running with this README file alone.  Be aware
that there's a wealth of other information in the doc directory, though, and
if you have problems or questions, that's the first place you should look
for answers.

As of Version 3.0, the basic CircleMUD source distribution compiles under
many variants of UNIX (SunOS 4.x, Solaris 2.x, Linux 1.x, IRIX 4.x/5.x,
Ultrix 4.x, HP/UX, and others), Windows 95/NT, and OS/2.

The Version 3.0 source does not currently compile under DOS, the Mac, or
the Amiga (although there is a binary distribution of Version 2.20 for the
Amiga).  If you are interested in creating a new port for any of these
platforms, see the file PORTING for some tips.

Read the file README.WIN for tips about running Circle 3.0 under Windows
and README.OS2 for tips about using OS/2.


1)  Download the latest version of CircleMUD.  You can always find the latest
    version at the following anonymous FTP sites:

        ftp.cs.jhu.edu:/pub/CircleMUD
        cambot.res.jhu.edu:/pub/CircleMUD

    You can also find information at the WWW site:

        http://www.cs.jhu.edu/~jelson/circle.html

    The archive is offered in two formats, one which ends in .tar.gz and
    one which ends in .zip.  The files have the same contents but have
    been compressed using different programs.  You should use the .tar.gz
    version if you're using UNIX or the .zip version if you're using
    Windows 95 or OS/2.


2)  Unpack the archive.  If you have the .tar.gz version, uncompress it using
    GNU gunzip and the tar archiver by typing this:

        gzip -dc circle30xxx.tar.Z | tar xvf -

    If you have the .zip version, make sure to use an UNZIP program capable of
    handling long filenames such as the UNIX UnZip program or WinZip for
    Windows 95.  Also, be sure to have your unzip program keep the directory
    structure of the archive instead of uncompressing all the files into the
    same directory.


The rest of the instructions apply ONLY to UNIX systems.  If you are using
a non-UNIX platform such as Windows or OS/2, stop here and read the README
file specific to your operating system instead (e.g. README.WIN, README.OS2,
etc.)


3)  Configure CircleMUD for your system.  Circle must be configured using
    the 'configure' program which attempts to guess correct values for
    various system-dependent variables used during compilation.  It uses
    those values to create a 'Makefile' and a header file called 'conf.h'.

    From Circle's root directory, type

        ./configure

    If you're using 'csh' on an old version of System V, csh might try to
    execute 'configure' itself, giving you a message like "Permission denied"
    when you try to run "./configure".  If so, type "sh ./configure" instead.

    'configure' can take several minutes if you're using a slow computer.


4)  Build the CircleMUD server.  This must be done from the 'src' directory.
    Type:
   
        cd src; make all

    This will build CircleMUD proper as well as its 10 or so ancillary
    utilities, which can take anywhere from 5 minutes to an hour depending
    on the speed of your computer.  Note that in the future, when you need
    to recompile Circle as you make changes to the code, it is NOT necessary
    to run 'configure' again (it should be run only once, after the first
    time you unpack Circle from its .tar file).

    The first time you try to compile Circle, you will be asked to read the
    CircleMUD license.  Please read it!


5)  Back in Circle's root directory, type 'autorun &' to start the server
    running in the background.  A file called 'syslog' will start growing
    that contains Circle's log messages (and boot messages).


6)  Wait until the line 'No connections.  Going to sleep.' appears in the
    syslog.  This indicates that the server is ready and waiting for
    connections.  It shouldn't take more than about 30 seconds for the MUD
    to reach this state, though performance will vary depending on how fast
    your computer is.

    If a file appears called 'syslog.CRASH', the MUD has terminated
    (probably abnormally).  Check the contents of syslog.CRASH to see
    what error was encountered.


7)  Type 'telnet localhost 4000' to connect.  The first person to log in
    will be made an implementor (level 34) with all powers.


Other Documentation
-------------------
If the 7-step guide above isn't enough to get you running, there's a lot
more information available.  All documentation (other than this file) is
in the /doc directory.

The README file in the /doc directory describes each documentation file
in detail, but there are several main files which should be of interest.
For the administrative side of mudding, running.doc gives an overall
description of how Circle works, how to get it to compile for the first
time and get it running, and hints on maintenance and day-to-day
administration.  For the coding side, coding.doc dives into Circle's
code and describes how to add new commands, spells, skills, socials,
classes, and whatnot.  For the builders in your group, building.doc
describes how to create new worlds, including rooms, objects, mobiles,
and shops.


Getting Help
------------
If you have strange problems -- and you can't figure out the answer by
reading the docs -- fear not, there are many other resources you can
turn to.  The best is probably the CircleMUD Mailing List, which you can
subscribe to by writing mail to "majordomo@pvv.unit.no" with a message
body of "subscribe circle".  If you want to write mail to the list, address
it to "circle@pvv.unit.no".  Over 200 CircleMUD imps read that list
regularly, so chances are you'll get help quickly.

If that doesn't work, you can always contact me directly by writing to
jelson@jhu.edu.  Or, take a look at the CircleMUD Home Page which is
at http://www.cs.jhu.edu/~jelson/circle.html, which may eventually
have some on-line documentation for Circle.

Finally if you have USENET access and are very brave, you can try posting
to the newsgroups rec.games.mud.diku or rec.games.mud.admin.

If you write mail asking for help, either to the mailing list or to me,
make sure to include the following information:

  -- The exact version of CircleMUD you're using (i.e. "CircleMUD 2.20",
     "CircleMUD 3.0 beta patchlevel 6", etc.).
  -- The EXACT text of any error messages, compiler errors, link errors,
     or any other errors you're getting.
  -- The exact type of hardware, operating system name and version, and
     compiler you're using.
  -- A description of ANY changes you've made, no matter how small, that
     might have contributed to the error.
  -- If you are having trouble getting Circle running for the very first
     time, also be sure to include the output of 'configure' and the file
     'config.log'.

Remember, I get dozens of pieces of email every day.  If you don't bother to
give me an excellent description of your problem, I will be somewhat annoyed
and will not be able to help you.  Here is a sample of a piece of mail I got:

     hi I need some help with CircleMUD....i tried compiling it on my system
     but I got all sorts of errors, and when i type bin/circle like it says
     in the manual it doesn't work.  Can you help????  you can log into my
     system if you want, the password is mud5.

Letters like that are usually ignored.  I get a lot of them!


Good luck, and have fun!

Jeremy Elson
aka Ras/Rasmussen
jelson@jhu.edu


USE OF THIS SOFTWARE IN ANY CAPACITY IMPLIES THAT YOU HAVE READ, UNDERSTOOD,
AND AGREED TO ABIDE BY THE TERMS AND CONDITIONS SET DOWN BY THE CIRCLEMUD
LICENSE.

