/*

   obj2to3.c - CircleMUD v2.2-to-v3.0 player file converter

   April 8th, 1996

   Konstantin Voloshin
   cat@aist.net
   
 */


#include "../conf.h"
#include "../sysdep.h"


#include "../structs.h"
#include "../comm.h"
#include "../handler.h"
#include "../db.h"
#include "../interpreter.h"
#include "../utils.h"
#include "../spells.h"

/* these factors should be unique integers */
#define RENT_FACTOR 	1
#define CRYO_FACTOR 	4



struct old_obj_file_elem {
   sh_int item_number;
   int	value[4];
   int	extra_flags;
   int	weight;
   int	timer;
   long	bitvector;
   struct obj_affected_type affected[2];
};



extern struct str_app_type str_app[];
extern struct room_data *world;
extern struct index_data *mob_index;
extern struct index_data *obj_index;
extern struct descriptor_data *descriptor_list;
extern struct player_index_element *player_table;
extern int top_of_p_table;
extern int min_rent_cost;


int	main( int argc, char **argv)
{
FILE	*fl, *fn;
char	fname[100], name[100];
int	gold, bank, i;
struct old_obj_file_elem	old_object;
struct obj_file_elem	object;
struct rent_info	rent;

  if ( argc < 2 )
  {
  	puts("Usage: obj2to3 <name>");
  	return 1;
  }
  sprintf( fname, "%s.objs", argv[1]);
  fl=  fopen ( fname, "rb" );
  if ( !fl )
  {
    puts("Error opening obj file !");
    exit(1);
  } 
  
  sprintf( fname, "%s.new", argv[1] );  
  fn = fopen ( fname, "wb" );
  if ( !fn )
  {
    puts("Error writing file !");
    exit(1);
  }
  

  fread ( &rent, sizeof(struct rent_info), 1, fl);
  fwrite ( &rent, sizeof(struct rent_info), 1, fn );

  i = 0;
  while (!feof(fl)) {
    fread(&old_object, sizeof(struct old_obj_file_elem), 1, fl);
    if (ferror(fl)) {
      perror("Reading crash file: Crash_load.");
      fclose(fl);
      exit (1);
    }
    
    memset( &object, 0, sizeof(struct obj_file_elem));
    object.item_number = old_object.item_number;
printf("\n%ld", object.item_number);
    object.value[0] = old_object.value[0];
printf("\t%ld", object.value[0]);
    object.value[1] = old_object.value[1];
printf("\t%ld", object.value[1]);
    object.value[2] = old_object.value[2];
printf("\t%ld", object.value[2]);
    object.value[3] = old_object.value[3];
printf("\t%ld", object.value[3]);
    object.extra_flags = old_object.extra_flags;
    object.weight = old_object.weight;
    object.timer = old_object.timer;
    object.bitvector = old_object.bitvector;
    object.affected[0].location = old_object.affected[0].location;
    object.affected[0].modifier = old_object.affected[0].modifier;
    object.affected[1].location = old_object.affected[1].location;
    object.affected[1].modifier = old_object.affected[1].modifier;
    
    if (!feof(fl))
      fwrite(&object, sizeof(struct obj_file_elem), 1, fn);
    i++;
  }
  fclose(fl);
/*
  rent.net_cost_per_diem = 0;
  rent.rentcode = RENT_CRASH;
  rent.time = time(0);
  rent.gold = gold;
  rent.account = bank;
  rent.nitems = i;
  rewind(fn);
  fwrite ( &rent, sizeof(struct rent_info), 1, fn );
*/
  fclose(fn);
  return 0;
 }
 
