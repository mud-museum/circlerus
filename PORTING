
                            Porting CircleMUD
                            -----------------

CircleMUD should work on most UNIX platforms without any modifications;
simply run the "configure" script and it should automatically detect what
type of system you have and anything that may be strange about it.  These
findings are all stored in a header file called conf.h which is created in
the src directory from a template called conf.h.in.  A Makefile is also
created from the template Makefile.in.

Non-UNIX platforms are a problem.  Some can't run CircleMUD at all.  However,
any multitasking OS that has an ANSI C compiler, and supports non-blocking
I/O and socket-based TCP/IP networking, should theoretically be able to run
CircleMUD (e.g. OS/2, Amiga, Mac, Windows 3.11/NT/95).

The port can be very easy or very difficult, depending mainly on whether or
nor your OS supports the Berkeley socket API.  Windows 95, for example, does
support Berkeley sockets with a few modifications using the WinSock library,
and OS/2 supports Berkeley sockets with no source changes at all.

The general steps for porting CircleMUD to a non-UNIX platform are listed
below.  Note that I have already ported Circle to Windows 95, so if you're
confused as to how to perform some of these steps, you can look at what I've
done as an example (see the files conf.h.win, Makefile.win and README.WIN).


Step 1.  Create a "conf.h" file for your system.  Copy the template
         "conf.h.in" to "conf.h", and then define or undefine each item
         as directed by the comments and based on the characteristics of
         your system.  To write a conf.h file, you'll need to know which
         header files are included with your system, the return type of
         signals, and various other pieces of information.

Step 2.  Create a Makefile.  Again, copy the template Makefile.in and make
         any changes which may be appropriate for your system.

Step 3.  Make the appropriate patches to the code so that the TCP/IP reads
         and writes and signal handling are compatible with your system.
         This is the hard part of porting Circle.  All of the changes you'll
         need to make will probably be in the source file comm.c.  As long
         as your system has an ANSI C compiler, the rest of the code should
         compile without any problems at all.

         IMPORTANT: Remember to keep any changes you make surrounded by
         #ifdef statements (i.e., "#ifdef CIRCLE_WINDOWS ... #endif").  If
         you make absolutely sure to mark all of your changes with #ifdef
         statements, then your patches (once you get them to work) will be
         suitable for incorporation into the CircleMUD distribution,
         meaning that CircleMUD will officially support your platform.

         A note about signals: Some systems don't support the concept of
         signals in the same way that UNIX does.  If you get stuck on
         porting the signal code (i.e. SIGUSR1, SIGHUP, etc.) then you
         can just #ifdef the entire thing out -- signals are not critical
         in Circle (they are only used for updating the wizlist and some
         other trivial things).  The Windows 95 port, for example, does not
         use signals at all.

Step 4.  Test your patches!  Make sure that multiple people can log in
         simultaneously and that they can all type commands at the same
         time.  No player should ever have a "frozen" screen just because
         another is waiting at a prompt.

Step 5.  If the patches work, celebrate!  Mail your conf.h, Makefile, diffs
         of comm.c to Jeremy Elson (jelson@jhu.edu) so that they can be
         included in future patches of CircleMUD.  Please share your patches
         so that other users of your OS can use Circle, too.  Also, be sure
         to submit a README file that describes everything that has to be
         done by another user of your system to get CircleMUD to compile;
         be sure to list any additional software or libraries that must be
         bought, etc.


Jeremy Elson
jelson@jhu.edu
